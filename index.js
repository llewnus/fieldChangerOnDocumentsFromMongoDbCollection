require("dotenv").config();
const { MongoClient } = require("mongodb");
const sDbURI = process.env.DB_URI;
const sDbName = process.env.DB_NAME;
const sCollectionName = process.env.DB_COLLECTION;
async function updateDocument(
  sDbURI,
  sDbName,
  sCollectionName,
  oFilter,
  oDocument
) {
  const uri = sDbURI;
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const database = client.db(sDbName);
    const collection = database.collection(sCollectionName);
    const filter = oFilter;
    const options = { upsert: true };
    const updateDoc = {
      $set: oDocument,
    };
    return await collection.updateOne(filter, updateDoc, options);
  } finally {
    await client.close();
  }
}

async function getDocumentsToUpdate(sDbURI, sDbName, sCollectionName) {
  const uri = sDbURI;
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const database = client.db(sDbName);
    const collection = database.collection(sCollectionName);
    return await collection.find({ tasa: { $ne: 0 } }).toArray();
  } finally {
    await client.close();
  }
}

async function run() {
  let aDocumentsToUpdate = await getDocumentsToUpdate(
    sDbURI,
    sDbName,
    sCollectionName
  );
  for (const key in aDocumentsToUpdate) {
    console.log(key);
    if (Object.hasOwnProperty.call(aDocumentsToUpdate, key)) {
      const element = aDocumentsToUpdate[key];
      console.log(element);
      let oFilter = { _id: element["_id"] };
      let oDocument = {
        tasa: element["tasa"] / 1000000,
      };
      await updateDocument(
        sDbURI,
        sDbName,
        sCollectionName,
        oFilter,
        oDocument
      );
    }
  }
}
run();
